# degooglfy

How to degooglfy your life.

In light of the recent "bans" ... "Alex Jones", "InfoWars", "Scott Horton -- an anti war activist", some of which appear to be coordinated, I am 
interested in fully removing my "persona" from these "platforms". While I do not follow these aforementioned "ban-ees" I believe my line has been 
'crossed'... I would like to say it was crossed even earlier than this, but there is already so much I am "dependent" on that doing 'something else' 
I perceive as a big pain point for me. Nonetheless, I am done being a 'product' and am willing to pay for theses services which I have been taking 
for "free". The big ones I intend to replace or eliminate are "gmail", "facebook", "google search" and "youtube" -- "twitter" I deactivated several 
years ago. "linkedin" I am willing to maintain as there is at the very least perceived value there for my employer. I can see others possibly seeing 
value in learning how to most efficiently de-productize themselves. I'm thinking about perhaps making a 'wiki' of how to go about achieving all of 
this.